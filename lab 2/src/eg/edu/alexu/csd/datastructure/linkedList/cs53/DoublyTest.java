package eg.edu.alexu.csd.datastructure.linkedList.cs53;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;
//import static org.junit.Assert.*;
import org.junit.Assert;
import org.junit.Test;
//import java.awt.*;

public class DoublyTest {
	doublyList testList = new doublyList();

	@Test(expected = NullPointerException.class)
	public void test1() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
			Assert.assertEquals("wrong", i, testList.get(i));
			Assert.assertNull("wrong", testList.get(i + 1));
		}
	}

	@Test(expected = NullPointerException.class)
	public void test2() {
		testList.add(0, 23);
		for (int i = 1; i < 11; i++) {
			testList.add(i);
		}
		// testList.add(0,23);
		testList.add(4, 10);
		testList.add(12, 500);
		Assert.assertEquals("wrong", 23, testList.get(0));
		Assert.assertEquals("wrong", 10, testList.get(4));
		Assert.assertEquals("wrong", 4, testList.get(5));
		Assert.assertEquals("wrong", 500, testList.get(12));
		Assert.assertEquals("wrong", 10, testList.get(11));
		Assert.assertNull("wrong", testList.get(13));
	}

	@Test(expected = NullPointerException.class)
	public void test3() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		testList.set(4, 10);
		testList.set(9, 500);
		testList.set(0, 522);
		Assert.assertEquals("wrong", 522, testList.get(0));
		Assert.assertEquals("wrong", 10, testList.get(4));
		Assert.assertEquals("wrong", 500, testList.get(9));
		Assert.assertEquals("wrong", 1, testList.get(1));
		Assert.assertNull("wrong", testList.get(11));
	}

	@Test(expected = NullPointerException.class)
	public void test4() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		ILinkedList newList = testList.sublist(2, 5);
		for (int i = 2; i <= 5; i++) {
			Assert.assertEquals("wrong", i, newList.get(i - 2));
		}
		Assert.assertNull("wrong", newList.get(4));

		newList = testList.sublist(0, 6);
		for (int i = 0; i <= 6; i++) {
			Assert.assertEquals("wrong", i, newList.get(i));
		}
		Assert.assertNull("wrong", newList.get(7));

		newList = testList.sublist(0, 0);
		Assert.assertEquals("wrong", 0, newList.get(0));
		Assert.assertNull("wrong", newList.get(1));

		newList = testList.sublist(10, 15);
		Assert.assertNull("wrong", newList.get(0));
		Assert.assertNull("wrong", newList.get(7));

		newList = testList.sublist(0, 9);
		for (int i = 0; i <= 9; i++) {
			Assert.assertEquals("wrong", i, newList.get(i));
		}
		Assert.assertNull("wrong", newList.get(10));

		newList = testList.sublist(0, 12);
		Assert.assertNull("wrong", newList.get(10));
	}

	@Test(expected = NullPointerException.class)
	public void test5() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		Assert.assertEquals("wrong", 10, testList.size());
		testList.remove(2);
		Assert.assertEquals("wrong", 3, testList.get(2));
		Assert.assertEquals("wrong", 9, testList.size());
		testList.remove(0);
		Assert.assertEquals("wrong", 3, testList.get(1));
		Assert.assertEquals("wrong", 8, testList.size());
		Assert.assertEquals("wrong", 1, testList.get(0));

		testList.remove(7);
		Assert.assertNull("wrong", testList.get(7));
		Assert.assertEquals("wrong", 8, testList.get(6));
		Assert.assertEquals("wrong", 7, testList.size());

		testList.remove(8);
		Assert.assertEquals("wrong", 8, testList.get(6));
		Assert.assertEquals("wrong", 7, testList.size());

		for (int i = 0; i < 7; i++) {
			testList.remove(0);
		}
		Assert.assertEquals("wrong", 0, testList.size());
	}

	@Test
	public void test6() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		Assert.assertFalse("wrong", testList.isEmpty());
		testList.clear();
		Assert.assertTrue("wrong", testList.isEmpty());
	}

	@Test
	public void test7() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		Assert.assertTrue("wrong", testList.contains(3));
		Assert.assertFalse("wrong", testList.contains(20));
	}
}
