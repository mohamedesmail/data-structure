package eg.edu.alexu.csd.datastructure.linkedList.cs53;

import eg.edu.alexu.csd.datastructure.linkedList.IPolynomialSolver;

public class app implements IPolynomialSolver {
	doublyList AList = new doublyList();
	doublyList BList = new doublyList();
	doublyList CList = new doublyList();
	doublyList RList = new doublyList();

	/**
	 * Set polynomial terms (coefficients & exponents)
	 * 
	 * @param poly
	 *            name of the polynomial
	 * @param terms
	 *            array of [coefficients][exponents]
	 */
	public void setPolynomial(char poly, int[][] terms) {
		for (int i = 0; i < terms.length; i++) {
			int temp = terms[i][1];
			for (int j = i; j < terms.length; j++) {
				if (temp < terms[j][1]||terms[j][1]<0) {
					throw null;
				}
			}
		}

		switch (poly) {
		case 'A':
			this.insertInList(AList, terms);
			break;
		case 'B':
			this.insertInList(BList, terms);
			break;
		case 'C':
			this.insertInList(CList, terms);
			break;
		case 'R':
			this.insertInList(RList, terms);
			break;
		default:
			throw null;
		}
	}

	public void insertInList(doublyList usedList, int[][] terms) {
		usedList.clear();
		for (int i = terms.length - 1, j = 0; i >= 0; i--, j++) {
			if (terms[i][1] == j) {
				usedList.add(terms[i][0]);
			} else {
				usedList.add(0);
				i++;
			}
		}
	}

	/**
	 * Print the polynomial in human readable representation
	 * 
	 * @param poly
	 *            name of the polynomial
	 * @return polynomial in the form like 27x^2+x-1
	 */
	public String print(char poly) {
		String out = new String();
		doublyList myList = new doublyList();
		switch (poly) {
		case 'A':
			myList = AList;
			break;
		case 'B':
			myList = BList;
			break;
		case 'C':
			myList = CList;
			break;
		case 'R':
			myList = RList;
			break;
		default:
			throw null;
		}
		if (myList.isEmpty()) {
			return null;
			// throw null;
		} else {
			if (!myList.isEmpty()) {
				switch (myList.size() - 1) {
				case 0:
					out = myList.get(myList.size() - 1).toString();
					break;
				case 1:
					if ((int) myList.get(myList.size() - 1) == 1) {
						out = "x";
					} else {
						out = myList.get(myList.size() - 1).toString() + "x";
					}
					break;
				default:
					out = myList.get(myList.size() - 1).toString() + "x^" + (myList.size() - 1);
				}
				for (int exp = (myList.size() - 2); exp >= 0; exp--) {
					int cof = (int) myList.get(exp);
					switch (exp) {
					case 0:
						if (cof != 0) {
							if (cof > 0) {
								out += "+" + cof;
							} else {
								out += cof;
							}
						}
						break;
					case 1:
						if (cof != 0) {
							if (cof == 1) {
								out += "+x";
							} else if (cof > 0) {
								out += "+" + cof + "x";
							} else {
								out += cof + "x";
							}
						}
						break;
					default:
						if (cof != 0) {
							if (cof == 1) {
								out += "+x^" + exp;
							} else if (cof > 0) {
								out += "+" + cof + "x^" + exp;
							} else {
								out += cof + "x^" + exp;
							}
						}
					}
				}
			}
			return out;
		}
	}

	/**
	 * Clear the polynomial
	 * 
	 * @param poly
	 *            name of the polynomial
	 */
	public void clearPolynomial(char poly) {
		doublyList myList = new doublyList();
		switch (poly) {
		case 'A':
			myList = AList;
			break;
		case 'B':
			myList = BList;
			break;
		case 'C':
			myList = CList;
			break;
		case 'R':
			myList = RList;
			break;
		default:
			throw null;
		}
		if (myList.isEmpty()) {
			throw null;
		} else {
			myList.clear();
		}
	}

	/**
	 * Evaluate the polynomial
	 * 
	 * @param poly
	 *            name of the polynomial
	 * @param the
	 *            polynomial constant value
	 * @return the value of the polynomial
	 */
	public float evaluatePolynomial(char poly, float value) {
		float result = 0;
		doublyList myList = new doublyList();
		switch (poly) {
		case 'A':
			myList = AList;
			break;
		case 'B':
			myList = BList;
			break;
		case 'C':
			myList = CList;
			break;
		case 'R':
			myList = RList;
			break;
		default:
			throw null;
		}
		if (myList.isEmpty()) {
			// return 0;
			throw null;
		} else {
			for (int exp = (myList.size() - 1); exp >= 0; exp--) {
				int cof = (int) myList.get(exp);
				result += (float) cof * (float) Math.pow(value, exp);
			}
			return result;
		}
	}

	/** ======================================================= */
	/**
	 * Add two polynomials
	 * 
	 * @param poly1
	 *            first polynomial
	 * @param poly2
	 *            second polynomial
	 * @return the result polynomial
	 */
	public int[][] add(char poly1, char poly2) {
		doublyList myList1 = new doublyList();
		doublyList myList2 = new doublyList();
		switch (poly1) {
		case 'A':
			myList1 = AList;
			break;
		case 'B':
			myList1 = BList;
			break;
		case 'C':
			myList1 = CList;
			break;
		default:
			throw null;
		}
		switch (poly2) {
		case 'A':
			myList2 = AList;
			break;
		case 'B':
			myList2 = BList;
			break;
		case 'C':
			myList2 = CList;
			break;
		default:
			throw null;
		}
		if (myList1.isEmpty() || myList2.isEmpty()) {
			throw null;
			// return null;
		} else {
			int i = 0;
			int cof2;
			int cof1;
			int[][] ADD;
			if (myList1.size() >= myList2.size()) {
				ADD = new int[myList1.size()][2];
				for (int exp = myList1.size() - 1; exp >= 0; exp--) {
					cof1 = (int) myList1.get(exp);
					if (exp < myList2.size())
						cof2 = (int) myList2.get(exp);
					else
						cof2 = 0;
					ADD[i][0] = cof1 + cof2;
					ADD[i][1] = exp;
					i++;
				}
			} else {
				ADD = new int[myList2.size()][2];
				for (int exp = myList2.size() - 1; exp >= 0; exp--) {
					cof2 = (int) myList2.get(exp);
					if (exp < myList1.size())
						cof1 = (int) myList1.get(exp);
					else
						cof1 = 0;
					ADD[i][0] = cof1 + cof2;
					ADD[i][1] = exp;
					i++;
				}
			}
			int[][] ADD1;
			int size = ADD.length;
			for (i = 0; i < ADD.length; i++) {
				if (ADD[i][0] == 0) {
					size--;
				}
			}
			ADD1 = new int[size][2];
			int j;
			for (i = 0, j = 0; i < ADD.length; i++) {
				if (ADD[i][0] != 0) {
					ADD1[j][0] = ADD[i][0];
					ADD1[j][1] = ADD[i][1];
					j++;
				}
			}
			this.setPolynomial('R', ADD1);
			return ADD1;
		}
	}

	/**
	 * Subtract two polynomials
	 * 
	 * @param poly1
	 *            first polynomial
	 * @param poly2
	 *            second polynomial
	 * @return the result polynomial
	 */
	public int[][] subtract(char poly1, char poly2) {
		doublyList myList1 = new doublyList();
		doublyList myList2 = new doublyList();
		if(poly1==poly2){
			int result[][]={{0,0}};
			return result;
		}
		switch (poly1) {
		case 'A':
			myList1 = AList;
			break;
		case 'B':
			myList1 = BList;
			break;
		case 'C':
			myList1 = CList;
			break;
		default:
			throw null;
		}
		switch (poly2) {
		case 'A':
			myList2 = AList;
			break;
		case 'B':
			myList2 = BList;
			break;
		case 'C':
			myList2 = CList;
			break;
		default:
			throw null;
		}
		if (myList1.isEmpty() || myList2.isEmpty()) {
			// return null;
			throw null;
		} else {
			int i = 0;
			int cof1, cof2;
			int[][] SUB;
			if (myList1.size() >= myList2.size()) {
				SUB = new int[myList1.size()][2];
				for (int exp = myList1.size() - 1; exp >= 0; exp--) {
					cof1 = (int) myList1.get(exp);
					if (exp < myList2.size())
						cof2 = (int) myList2.get(exp);
					else
						cof2 = 0;
					SUB[i][0] = cof1 - cof2;
					SUB[i][1] = exp;
					i++;
				}
			} else {
				SUB = new int[myList2.size()][2];
				for (int exp = myList2.size() - 1; exp >= 0; exp--) {
					cof2 = (int) myList2.get(exp);
					if (exp < myList1.size())
						cof1 = (int) myList1.get(exp);
					else
						cof1 = 0;
					SUB[i][0] = cof1 - cof2;
					SUB[i][1] = exp;
					i++;
				}
			}
			int[][] SUB1;
			int size = SUB.length;
			for (i = 0; i < SUB.length; i++) {
				if (SUB[i][0] == 0) {
					size--;
				}
			}
			SUB1 = new int[size][2];
			int j;
			for (i = 0, j = 0; i < SUB.length; i++) {
				if (SUB[i][0] != 0) {
					SUB1[j][0] = SUB[i][0];
					SUB1[j][1] = SUB[i][1];
					j++;
				}
			}
			this.setPolynomial('R', SUB1);
			return SUB1;
		}
	}

	/**
	 * Multiply two polynomials * @param poly1 first polynomial
	 * 
	 * @param poly2
	 *            second polynomial
	 * @return the result polynomial
	 */
	public int[][] multiply(char poly1, char poly2) {
		doublyList myList1 = new doublyList();
		doublyList myList2 = new doublyList();
		switch (poly1) {
		case 'A':
			myList1 = AList;
			break;
		case 'B':
			myList1 = BList;
			break;
		case 'C':
			myList1 = CList;
			break;
		default:
			throw null;
		}
		switch (poly2) {
		case 'A':
			myList2 = AList;
			break;
		case 'B':
			myList2 = BList;
			break;
		case 'C':
			myList2 = CList;
			break;
		default:
			throw null;
		}

		if (myList1.isEmpty() || myList2.isEmpty()) {
			throw null;
		} else {
			int exp1, exp2, cof1, cof2;
			int[][] mult;
			int[][] mult1;
			mult = new int[myList1.size() + myList2.size() - 1][2];
			mult1 = new int[myList1.size() + myList2.size() - 1][2];
			for (int i = 0; i < myList1.size() + myList2.size() - 1; i++) {
				for (int j = 0; j < 2; j++) {
					mult[i][j] = 0;
				}
			}
			for (int i = 0; i < myList1.size() + myList2.size() - 1; i++) {
				for (int j = 0; j < 2; j++) {
					mult1[i][j] = 0;
				}
			}
			for (exp1 = myList1.size() - 1; exp1 >= 0; exp1--) {
				for (exp2 = myList2.size() - 1; exp2 >= 0; exp2--) {
					cof1 = (int) myList1.get(exp1);
					cof2 = (int) myList2.get(exp2);
					mult[exp1 + exp2][0] += cof1 * cof2;
					mult[exp1 + exp2][1] = exp1 + exp2;
				}
			}
			int j = myList1.size() + myList2.size() - 2;
			for (int i = 0; i < myList1.size() + myList2.size() - 1; i++) {
				mult1[i][0] = mult[j][0];
				mult1[i][1] = mult[j][1];
				j--;
			}
			int[][] mult2;
			int size = mult1.length;
			int i = 0;
			for (i = 0; i < mult1.length; i++) {
				if (mult1[i][0] == 0) {
					size--;
				}
			}
			mult2 = new int[size][2];
			for (i = 0, j = 0; i < mult1.length; i++) {
				if (mult1[i][0] != 0) {
					mult2[j][0] = mult1[i][0];
					mult2[j][1] = mult1[i][1];
					j++;
				}
			}
			this.setPolynomial('R', mult2);
			return mult2;
		}
	}
}
