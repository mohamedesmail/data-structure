package eg.edu.alexu.csd.datastructure.linkedList.cs53;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

public class SinglyLinkedList implements ILinkedList {

	public class Node {
		public Object data;
		public Node next;
	}

	public Node head;
	public Node current;
	public Node tail;
	public int size;

	public void add(int index, Object element) {

		this.size = size();
		if (!(index < 0 || index > this.size)) {
			Node n = new Node();
			n.data = element;
			if (isEmpty()) {
				this.head = n;
				this.tail = n;
				n.next = null;
				this.size++;
			} else if (index == this.size) {
				add(element);
			} else if (index == 0) {
				n.next = this.head;
				this.head = n;
				this.size++;
			} else {
				this.current = this.head;
				for (int i = 0; i < this.size; i++) {
					if (i == index - 1) {
						Node temp = new Node();
						temp = this.current.next;
						this.current.next = n;
						n.next = temp;
						this.size++;
						break;
					}
					this.current = this.current.next;
				}
			}
		} else {
			throw null;
		}
	}

	public void add(Object element) {
		this.size = size();
		Node n = new Node();
		n.data = element;
		if (isEmpty()) {
			this.head = n;
			n.next = null;
			this.size++;
			this.tail = n;
		} else {
			n.next = null;
			this.tail.next = n;
			this.tail = n;
			this.size++;
		}
	}

	public Object get(int index) {
		this.size = size();
		if (this.head == null)
			return null;
		this.current = this.head;
		for (int i = 0; i < this.size; i++) {
			if (i == index) {
				break;
			}
			this.current = this.current.next;
		}
		if (this.current == null)
			throw null;

		return this.current.data;
	}

	public void set(int index, Object element) {
		if (!(index < 0 || index > this.size)) {
			this.size = size();
			this.current = this.head;
			for (int i = 0; i < this.size; i++) {
				if (i == index) {
					this.current.data = element;
					break;
				}
				this.current = this.current.next;
			}
		} else {
			throw null;
		}
	}

	public void clear() {
		this.head = null;
	}

	public boolean isEmpty() {
		return this.head == null;
	}

	public void remove(int index) {
		this.size = size();
		if (!isEmpty()) {
			if (index == 0) {
				this.head = this.head.next;
				this.size--;
				if (this.head == null)
					this.tail = null;
			} else if (index == this.size - 1) {
				this.current = this.head;
				for (int i = 0; i < this.size; i++) {
					if (i == index - 1) {
						break;
					}
					this.current = this.current.next;
				}
				this.tail = this.current;
				this.tail.next = null;
				this.size--;
			} else if ((index > this.size - 1) || (index < 0)) {
				throw null;
			} else {
				Node n = new Node();
				this.current = this.head;
				for (int i = 0; i < this.size; i++) {
					if (i == index - 1) {
						n = this.current.next;
						this.current.next = n.next;
						n = null;
						this.size--;
						break;
					}
					this.current = this.current.next;
				}
			}
		} else {
			throw null;
		}
	}

	public int size() {
		int size = 0;
		this.current = this.head;
		while (this.current != null) {
			size++;
			this.current = this.current.next;
		}
		return size;
	}

	public ILinkedList sublist(int fromIndex, int toIndex) {
		SinglyLinkedList list = new SinglyLinkedList();
		this.size = size();
		if (fromIndex < 0 || fromIndex > this.size || toIndex < fromIndex || toIndex > this.size)
			return null;
		this.current = new Node();
		this.current.data = this.head.data;
		this.current.next = new Node();
		Node x = this.head;
		for (int i = 0; i < fromIndex; i++) {
			this.current.data = x.next.data;
			x = x.next;
		}
		list.add(this.current.data);
		for (int i = fromIndex + 1; i <= toIndex; i++) {
			this.current.next.data = x.next.data;
			list.add(this.current.next.data);
			x = x.next;
			this.current = this.current.next;
			current.next = new Node();

		}
		list.tail.next = null;
		return list;
	}

	public boolean contains(Object o) {
		this.size = size();
		this.current = this.head;
		for (int i = 0; i < this.size; i++) {
			if (this.current.data.equals(o))
				return true;
			this.current = this.current.next;
		}
		return false;
	}
}
