package eg.edu.alexu.csd.datastructure.stack.cs53;

import java.util.Scanner;

public class MainTest {

	private static Scanner input;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyStack x = new MyStack();

		input = new Scanner(System.in);

		int op;
		Object element;
		while (true) {
			System.out.println("Choose the operatin and to end the program enter negtive number");
			System.out.println("1: Push");
			System.out.println("2: Pop");
			System.out.println("3: Peek");
			System.out.println("4: Get size");
			System.out.println("5: Check if empty");
			op = input.nextInt();
			if (op < 0) {
				break;
			}
			switch (op) {
			case 1:
				System.out.println("Enter the input");
				element = input.next();
				x.push(element);
				break;
			case 2:
				try {
					element = x.pop();
					System.out.println(element);
				} catch (Exception e) {
					System.out.println("stack is empty");
				}
				break;
			case 3:
				try {
					element = x.peek();
					System.out.println(element);
				} catch (Exception e) {
					System.out.println("stack is empty");
				}
				break;
			case 4:
				int size = x.size();
				System.out.println(size);
				break;
			case 5:
				boolean empty = x.isEmpty();
				if (empty) {
					System.out.println("Stack is empty");
				} else {
					System.out.println("Stack is not empty");
				}
				break;
			default:
				System.out.println("wrong operation");
			}
		}
	}

}
