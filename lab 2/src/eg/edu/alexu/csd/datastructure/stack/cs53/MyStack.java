package eg.edu.alexu.csd.datastructure.stack.cs53;

import eg.edu.alexu.csd.datastructure.stack.IStack;

public class MyStack implements IStack {
	public final int max = 100002;
	private Object[] arr = new Object[max];
	private int size;
	private int topIndex = -1;

	/**
	 * Inserts a specified element at the specified position in the list.
	 * 
	 * @param index
	 *            zero-based index
	 * @param element
	 *            object to insert
	 */
	public void add(int index, Object element) {
		this.size = this.size();
		if (index > this.size || index < 0) {
			throw new IndexOutOfBoundsException();
		} else if (index == this.size) {
			this.push(element);
		} else if (index >= 0 && index < this.size) {
			int i;
			for (i = this.size; i > index; i--) {
				this.arr[i] = this.arr[i - 1];
			}
			this.arr[index] = element;
			this.topIndex++;
		}
	}

	/**
	 * Removes the element at the top of stack and returns that element.
	 * 
	 * @return top of stack element, or through exception if empty
	 */
	public Object pop() {
		Object out;
		if (this.isEmpty()) {
			throw null;
			// return null;
		} else {
			out = this.arr[this.topIndex];
			this.topIndex--;
			return out;
		}

	}

	/**
	 * Get the element at the top of stack without removing it from stack.
	 * 
	 * @return top of stack element, or through exception if empty
	 */
	public Object peek() {
		Object out;
		if (this.isEmpty()) {
			throw null;
		} else {
			out = this.arr[this.topIndex];
		}
		return out;
	}

	/**
	 * Pushes an item onto the top of this stack.
	 * 
	 * @param object
	 *            to insert
	 */
	public void push(Object element) {
		this.size = this.size();
		if (this.size == this.max) {
			throw null;
		} else {
			this.topIndex++;
			this.arr[this.topIndex] = element;
		}
	}

	/**
	 * Tests if this stack is empty
	 * 
	 * @return true if stack empty
	 */
	public boolean isEmpty() {
		return (this.topIndex < 0);
	}

	/**
	 * Returns the number of elements in the stack.
	 * 
	 * @return number of elements in the stack
	 */
	public int size() {
		return this.topIndex + 1;
	}

}
