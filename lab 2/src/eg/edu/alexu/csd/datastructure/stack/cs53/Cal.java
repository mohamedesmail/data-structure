package eg.edu.alexu.csd.datastructure.stack.cs53;

import eg.edu.alexu.csd.datastructure.stack.IExpressionEvaluator;

public class Cal implements IExpressionEvaluator {
	private MyStack stack = new MyStack();

	/**
	 * Takes a symbolic/numeric infix expression as input and converts it to
	 * postfix notation. There is no assumption on spaces between terms or the
	 * length of the term (e.g., two digits symbolic or numeric term)
	 * 
	 * @param expression
	 *            infix expression
	 * @return postfix expression
	 */
	public String infixToPostfix(String expression) {
		if (expression.length() == 0 || expression == null || expression.equals("")) {
			throw null;
		}
		String newExp = new String();
		for (int i = 0; i < expression.length(); i++) {
			char c = expression.charAt(i);
			if (c != ' ') {
				newExp += c;
			}
		}

		String out = new String();
		for (int i = 0; i < newExp.length(); i++) {
			char c = newExp.charAt(i);
			if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z')) {
				out += c;
			} else if (c == '(') {
				stack.push(c);
			} else if (c == ')') {
				out += " ";
				while (true) {
					c = (char) stack.pop();
					if (c == '(') {
						break;
					} else {
						out += c + " ";
					}
				}
			} else if (c == '+' || c == '-' || c == '*' || c == '/') {
				char x = newExp.charAt(i + 1);
				if (x == '+' || x == '-' || x == '*' || x == '/') {
					throw null;
				}
				if (!(newExp.charAt(i - 1) == ')')) {
					out += " ";
				}
				if (stack.isEmpty()) {
					stack.push(c);
				} else {
					char temp = (char) stack.peek();
					if (temp == '(') {
						stack.push(c);
					} else if (c == '+' || c == '-') {
						while (!stack.isEmpty() && temp != '(') {
							temp = (char) stack.pop();
							out += temp + " ";
							if (!stack.isEmpty()) {
								temp = (char) stack.peek();
							}
						}
						stack.push(c);
					} else if (c == '*' || c == '/') {
						if (temp == '*' || temp == '/') {
							temp = (char) stack.pop();
							out += temp + " ";
						}
						stack.push(c);
					}
				}
			}
		}
		while (!stack.isEmpty()) {
			char c = (char) stack.pop();
			if (c == '(') {
				throw null;
			} else {
				out += " " + c;
			}
		}
		return out;
	}

	/**
	 * Evaluate a postfix numeric expression, with a single space separator
	 * 
	 * @param expression
	 *            postfix expression
	 * @return the expression evaluated value
	 */
	public int evaluate(String expression) {
		if (expression.length() == 0 || expression == null || expression.equals("")) {
			throw null;
		}
		float result = 0, num1, num2;
		String temp = new String(), temp1;
		char c;
		int i, num;
		for (i = 0; i < expression.length(); i++) {
			c = expression.charAt(i);
			if (c >= '0' && c <= '9') {
				temp += c;
			} else if (c == ' ') {
				if (!temp.equals("")) {
					if (temp.charAt(0) >= '0' && temp.charAt(0) <= '9') {
						stack.push(temp);
						temp = new String();
					}
				}
			} else if (c == '+' || c == '-' || c == '*' || c == '/') {
				temp1 = stack.pop().toString();
				num2 = Float.parseFloat(temp1);
				temp1 = stack.pop().toString();
				num1 = Float.parseFloat(temp1);
				switch (c) {
				case '+':
					result = num1 + num2;
					stack.push(result);
					break;
				case '-':
					result = num1 - num2;
					stack.push(result);
					break;
				case '*':
					result = num1 * num2;
					stack.push(result);
					break;
				case '/':
					result = num1 / num2;
					stack.push(result);
					break;
				}
			}
		}
		result = (float) stack.pop();
		return Math.round(result);
	}
}
