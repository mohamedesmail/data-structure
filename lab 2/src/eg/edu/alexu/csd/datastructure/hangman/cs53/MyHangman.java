package eg.edu.alexu.csd.datastructure.hangman.cs53;


import java.util.Random;
import eg.edu.alexu.csd.datastructure.hangman.IHangman;

public class MyHangman implements IHangman  {
	private String [] dictionary ;
	private String SecretWord ;
	//private char [] WordArr = new char[20] ;
	private String UserWord;	
	private int MaxWrongs ,counter=0;
	
	/***************/
	
	public void setDictionary(String[] words){		
		dictionary =new String[words.length];
		dictionary = words;
	}
	
	
	
     /****************/
	public String selectRandomSecretWord(){
		if (dictionary !=null){
			Random rand = new Random();
			int i =dictionary.length;
			int  n = rand.nextInt(i);
			
			SecretWord = dictionary [n];
			
			SecretWord =SecretWord.toUpperCase();
			char [] WordArr = new char[SecretWord.length()];
			for(int j=0 ;j<SecretWord.length();j++){
				WordArr[j]= '-';
			}
			UserWord=String.valueOf(WordArr);			
		}
		
		return SecretWord;		
	}
	
	
	/****************************/
	public String guess(Character c){
		
		char [] WordArr = new char[SecretWord.length()];
		if (UserWord != null){
			WordArr=UserWord.toCharArray();
			c = Character.toUpperCase(c);
			
			boolean correct = false;
			if (c != null){
				for (int i=0;i<SecretWord.length() ;i++){
					
					if(SecretWord.charAt(i)== c){
						WordArr[i] = c;
						correct = true ; 
					}
				}
				UserWord = String.valueOf(WordArr);
			}
			
			if (!correct){
				counter++;
			}
			
			if (counter == MaxWrongs){
				UserWord =null;
			}
		}
		
		return UserWord;
	}
	
	
/***************************************************/
	


	public void setMaxWrongGuesses(Integer max) {
		// TODO Auto-generated method stub
		if (max==null){
			MaxWrongs=0;
		}else{
			MaxWrongs=max;
		}
	}
	
	
}
