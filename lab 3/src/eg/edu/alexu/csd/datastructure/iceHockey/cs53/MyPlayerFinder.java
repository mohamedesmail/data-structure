package eg.edu.alexu.csd.datastructure.iceHockey.cs53;

import eg.edu.alexu.csd.datastructure.iceHockey.IPlayersFinder;
import java.awt.*;

public class MyPlayerFinder implements IPlayersFinder {
	
	private int  right,left, up, down ,counter = 0;
	public int NoPlayers = 0;
	private char []  photoArr ;
	
	/**
	* Search for players locations at the given photo
	* @param photo
	* Two dimension array of photo contents
	* Will contain between 1 and 50 elements, inclusive
	* @param team
	* Identifier of the team
	* @param threshold
	* Minimum area for an element
	* Will be between 1 and 10000, inclusive
	* @return
	* Array of players locations of the given team
	*/
	public java.awt.Point[]findPlayers(String[] photo,int team,int threshold){
		
		java.awt.Point [] arr = new java.awt.Point[1000] ;
		java.awt.Point [] finalArr = null ;
	    if (photo != null){
			for(int column = 0; column<photo[0].length() ;column++){
		    	for  (int row = 0 ; row<photo.length ;row++){
		    		if (photo[row].charAt(column) == (char) (team+'0')){
		    			this.right = (column+1)*2;
		    			this.left = this.right-2;
		    			this.down = (row+1)*2;
		    			this.up = this.down-2;
		    			
		    			this.counter = this.countCells(photo,row,column,team);
		    			
		    			if( threshold <= counter*4){
		    				int columns = (this.right+this.left)/2;
		    				int rows = (this.up+this.down)/2;
		    				arr[NoPlayers]=new java.awt.Point();
		    				arr[NoPlayers].setLocation(columns, rows);
		    				NoPlayers++;
		    			}
		    			this.counter = 0;
		    		}
		    	}
		    	
		    }
			finalArr =new java.awt.Point[NoPlayers] ;
			for(int j =0 ;j<NoPlayers ; j++){
				finalArr[j] = arr[j];
			}
			finalArr = this.sort(arr);
	    }
		return finalArr ;
	}
		
		
		private int countCells(String[] photo,int row,int column, int team){		
			this.counter++;
			photoArr = new char[photo[row].length()];
			photoArr = photo[row].toCharArray();
			photoArr[column] = '*';
			photo[row] = String.valueOf(photoArr);
			
			if( (column+1<photo[0].length()) &&(photo[row].charAt(column+1) == (char)(team+'0')) ){			
				if(this.right < ((column+1)+1)*2){
					this.right = this.right +2;
				}
				this.countCells(photo,row,column+1,team);
			}
			
			if(  (row-1 >=0)&&(photo[row-1].charAt(column) == (char)(team+'0')) ){			
				if(this.up > ((row-1)+1)*2 -2){
					this.up = this.up -2;
				}
				this.countCells(photo,row-1,column,team);
			}
			
			if((row+1<photo.length)&&(photo[row+1].charAt(column) == (char)(team+'0')) ){			
				if(this.down < ((row+1)+1)*2){
					this.down = this.down +2;
				}
				this.countCells(photo,row+1,column,team);
			}
			if( (column-1 >= 0) &&(photo[row].charAt(column-1) == (char)(team+'0')) ){			
				if(this.left > ((column-1)+1)*2 -2){
					this.left = this.left -2;
				}
				this.countCells(photo,row,column-1,team);
			}
			
		return this.counter;
	}
	
/*************************************/
	
	private java.awt.Point []  sort(java.awt.Point [] arr){
		//java.awt.Point [] finalArr =new java.awt.Point[NoPlayers] ;		
		int i=0;
		if (arr != null){
			while (i < this.NoPlayers-1){
				if(arr [i] != null && arr[i].x > arr[i+1].x){
					Point temp = arr[i];
					arr[i] = arr[i+1];
					arr[i+1] = temp;
					if(i != 0){
						i--;
					}
				}else {
					i++;				
				}				
			}
			
			i=0;
			while(i< this.NoPlayers-1){
				if(arr [i] != null && arr[i].x == arr[i+1].x){
					if(arr[i].y > arr[i+1].y){
						Point temp = arr[i];
						arr[i] = arr[i+1];
						arr[i+1] = temp;
						if(i != 0){
							i--;
						}
					}else{
						i++;
					}
				}else{
					i++;
				}
			}
			/*for(int j =0 ;j<NoPlayers ; j++){
				finalArr[j] = arr[j];
			}*/
		}		
		return arr;
	}
}


