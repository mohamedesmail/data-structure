package eg.edu.alexu.csd.datastructure.linkedList.cs53_64;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		app solve = new app();
		while (true) {
			System.out.println("Please choose an action\n");
			System.out.println("-----------------------\n");
			System.out.println("1 - Set a polynomial variable\n");
			System.out.println("2 - Print the value of a polynomial variable\n");
			System.out.println("3 - Add two polynomials\n");
			System.out.println("4 - Subtract two polynomials\n");
			System.out.println("5 - Multiply two polynomials\n");
			System.out.println("6 - Evaluate a polynomial at some point\n");
			System.out.println("7 - Clear a polynomial variable\n");
			System.out.println("8 - Exit the program\n");
			System.out.println("====================================================================\n");
			Scanner s = new Scanner(System.in);
			int choice = s.nextInt();
			switch (choice) {
			case 8:
				System.exit(0);
			case 1: {
				System.out.println("Insert the variable name : A , B or C\n");
				char poly = s.next().charAt(0);
				int i = 0;
				int[][] value = new int[100][2];
				System.out.println("Insert the polynomial terms in the form :\n");
				System.out
						.println("( coeff1 , exponent1 ) , ( coeff2 , exponent2 ) , ..or (-1,-1) at the end to exit\n");
				s.useDelimiter("[\\s\\(\\),]+");
				do {
					int x = s.nextInt();
					int y = s.nextInt();
					if (y < 0)
						break;
					value[i][0] = x;
					value[i][1] = y;
					i++;
				} while (true);
				int[][] value1 = new int[i][2];
				for (int j = 0; j < i; j++) {
					value1[j][0] = value[j][0];
					value1[j][1] = value[j][1];
				}
				solve.setPolynomial(poly, value1);
				break;
			}
			case 2: {
				System.out.println("Insert the variable name : A , B , C or R\n");
				char poly = s.next().charAt(0);
				String result = solve.print(poly);
				if (result == null) {
					System.out.println(poly + " is not set.\n");
					break;
				}
				System.out.println("Value in " + poly + ": " + result + "\n");
				break;
			}
			case 3: {
				System.out.println("Insert first operand variable name : A , B or C\n");
				char poly1 = s.next().charAt(0);
				String result = solve.print(poly1);
				if (result == null) {
					System.out.println("variable not set.\n");
					break;
				}
				System.out.println("Insert Second operand variable name : A , B or C\n");
				char poly2 = s.next().charAt(0);
				result = solve.print(poly2);
				if (result == null) {
					System.out.println("variable not set.\n");
					break;
				}
				int[][] result1 = solve.add(poly1, poly2);
				System.out.println("Result set in R : ");
				for (int i = 0; i < result1.length; i++) {
					System.out.println("(" + result1[i][0] + ", " + result1[i][1] + ")");
					if (i < result1.length - 1)
						System.out.println(", ");
				}
				break;
			}
			case 4: {
				System.out.println("Insert first operand variable name : A , B or C\n");
				char poly1 = s.next().charAt(0);
				String result = solve.print(poly1);
				if (result == null) {
					System.out.println("variable not set.\n");
					break;
				}
				System.out.println("Insert Second operand variable name : A , B or C\n");
				char poly2 = s.next().charAt(0);
				result = solve.print(poly2);
				if (result == null) {
					System.out.println("variable not set.\n");
					break;
				}
				int[][] result1 = solve.subtract(poly1, poly2);
				System.out.println("Result set in R : ");
				for (int i = 0; i < result1.length; i++) {
					System.out.println("(" + result1[i][0] + ", " + result1[i][1] + ")");
					if (i < result1.length - 1)
						System.out.println(", ");
				}
				break;
			}
			case 5: {
				System.out.println("Insert first operand variable name : A , B or C\n");
				char poly1 = s.next().charAt(0);
				String result = solve.print(poly1);
				if (result == null) {
					System.out.println("variable not set.\n");
					break;
				}
				System.out.println("Insert Second operand variable name : A , B or C\n");
				char poly2 = s.next().charAt(0);
				result = solve.print(poly2);
				if (result == null) {
					System.out.println("variable not set.\n");
					break;
				}
				int[][] result1 = solve.multiply(poly1, poly2);
				System.out.println("Result set in R : ");
				for (int i = 0; i < result1.length; i++) {
					System.out.println("(" + result1[i][0] + ", " + result1[i][1] + ")");
					if (i < result1.length - 1)
						System.out.println(", ");
				}
				break;
			}
			case 6: {
				System.out.println("Insert variable name : A , B or C\n");
				char poly = s.next().charAt(0);
				String result = solve.print(poly);
				if (result == null) {
					System.out.println(poly + " is not set.\n");
					break;
				}
				System.out.println("Insert value of x\n");
				float value = s.nextFloat();
				float result1 = solve.evaluatePolynomial(poly, value);
				System.out.println("Result of " + poly + " when x = " + value + " is " + result1 + "\n");
				break;
			}
			case 7: {
				System.out.println("Insert variable name : A , B or C\n");
				char poly = s.next().charAt(0);
				try {
					solve.clearPolynomial(poly);
				} catch (Exception e) {
					System.out.println("variable is already empty.\n");
				}
				break;
			}

			}
		}
	}
}
