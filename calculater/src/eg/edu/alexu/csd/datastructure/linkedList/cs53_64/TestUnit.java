package eg.edu.alexu.csd.datastructure.linkedList.cs53_64;

import org.junit.Assert;
import org.junit.Test;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

public class TestUnit {

	doublyList testList = new doublyList();
	SinglyLinkedList testList2 = new SinglyLinkedList();

	@Test(expected = NullPointerException.class)
	public void test1() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
			Assert.assertEquals("wrong", i, testList.get(i));
			Assert.assertNull("wrong", testList.get(i + 1));
		}
		for (int i = 0; i < 10; i++) {
			testList2.add(i);
			Assert.assertEquals("wrong", i, testList2.get(i));
			Assert.assertNull("wrong", testList2.get(i + 1));
		}
	}

	@Test(expected = NullPointerException.class)
	public void test2() {
		testList.add(0, 23);
		for (int i = 1; i < 11; i++) {
			testList.add(i);
		}
		testList.add(4, 10);
		testList.add(12, 500);
		Assert.assertEquals("wrong", 23, testList.get(0));
		Assert.assertEquals("wrong", 10, testList.get(4));
		Assert.assertEquals("wrong", 4, testList.get(5));
		Assert.assertEquals("wrong", 500, testList.get(12));
		Assert.assertEquals("wrong", 10, testList.get(11));
		Assert.assertNull("wrong", testList.get(13));
		/** ================================================ */
		testList2.add(0, 23);
		for (int i = 1; i < 11; i++) {
			testList2.add(i);
		}
		testList2.add(4, 10);
		testList2.add(12, 500);
		Assert.assertEquals("wrong", 23, testList2.get(0));
		Assert.assertEquals("wrong", 10, testList2.get(4));
		Assert.assertEquals("wrong", 4, testList2.get(5));
		Assert.assertEquals("wrong", 500, testList2.get(12));
		Assert.assertEquals("wrong", 10, testList2.get(11));
		Assert.assertNull("wrong", testList2.get(13));
	}

	@Test(expected = NullPointerException.class)
	public void test3() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		testList.set(4, 10);
		testList.set(9, 500);
		testList.set(0, 522);
		Assert.assertEquals("wrong", 522, testList.get(0));
		Assert.assertEquals("wrong", 10, testList.get(4));
		Assert.assertEquals("wrong", 500, testList.get(9));
		Assert.assertEquals("wrong", 1, testList.get(1));
		Assert.assertNull("wrong", testList.get(11));
		/** ======================================================= */
		for (int i = 0; i < 10; i++) {
			testList2.add(i);
		}
		testList2.set(4, 10);
		testList2.set(9, 500);
		testList2.set(0, 522);
		Assert.assertEquals("wrong", 522, testList2.get(0));
		Assert.assertEquals("wrong", 10, testList2.get(4));
		Assert.assertEquals("wrong", 500, testList2.get(9));
		Assert.assertEquals("wrong", 1, testList2.get(1));
		Assert.assertNull("wrong", testList2.get(11));
	}

	@Test(expected = NullPointerException.class)
	public void test4() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		ILinkedList newList = testList.sublist(2, 5);
		for (int i = 2; i <= 5; i++) {
			Assert.assertEquals("wrong", i, newList.get(i - 2));
		}
		Assert.assertNull("wrong", newList.get(4));

		newList = testList.sublist(0, 6);
		for (int i = 0; i <= 6; i++) {
			Assert.assertEquals("wrong", i, newList.get(i));
		}
		Assert.assertNull("wrong", newList.get(7));

		newList = testList.sublist(0, 0);
		Assert.assertEquals("wrong", 0, newList.get(0));
		Assert.assertNull("wrong", newList.get(1));

		newList = testList.sublist(10, 15);
		Assert.assertNull("wrong", newList.get(0));
		Assert.assertNull("wrong", newList.get(7));

		newList = testList.sublist(0, 9);
		for (int i = 0; i <= 9; i++) {
			Assert.assertEquals("wrong", i, newList.get(i));
		}
		Assert.assertNull("wrong", newList.get(10));

		newList = testList.sublist(0, 12);
		Assert.assertNull("wrong", newList.get(10));
		/** ===================================================== */
		for (int i = 0; i < 10; i++) {
			testList2.add(i);
		}
		newList = testList2.sublist(2, 5);
		for (int i = 2; i <= 5; i++) {
			Assert.assertEquals("wrong", i, newList.get(i - 2));
		}
		Assert.assertNull("wrong", newList.get(4));

		newList = testList2.sublist(0, 6);
		for (int i = 0; i <= 6; i++) {
			Assert.assertEquals("wrong", i, newList.get(i));
		}
		Assert.assertNull("wrong", newList.get(7));

		newList = testList2.sublist(0, 0);
		Assert.assertEquals("wrong", 0, newList.get(0));
		Assert.assertNull("wrong", newList.get(1));

		newList = testList2.sublist(10, 15);
		Assert.assertNull("wrong", newList.get(0));
		Assert.assertNull("wrong", newList.get(7));

		newList = testList2.sublist(0, 9);
		for (int i = 0; i <= 9; i++) {
			Assert.assertEquals("wrong", i, newList.get(i));
		}
		Assert.assertNull("wrong", newList.get(10));

		newList = testList2.sublist(0, 12);
		Assert.assertNull("wrong", newList.get(10));

	}

	@Test(expected = NullPointerException.class)
	public void test5() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		Assert.assertEquals("wrong", 10, testList.size());
		testList.remove(2);
		Assert.assertEquals("wrong", 3, testList.get(2));
		Assert.assertEquals("wrong", 9, testList.size());
		testList.remove(0);
		Assert.assertEquals("wrong", 3, testList.get(1));
		Assert.assertEquals("wrong", 8, testList.size());
		Assert.assertEquals("wrong", 1, testList.get(0));

		testList.remove(7);
		Assert.assertNull("wrong", testList.get(7));
		Assert.assertEquals("wrong", 8, testList.get(6));
		Assert.assertEquals("wrong", 7, testList.size());

		testList.remove(8);
		Assert.assertEquals("wrong", 8, testList.get(6));
		Assert.assertEquals("wrong", 7, testList.size());

		for (int i = 0; i < 7; i++) {
			testList.remove(0);
		}
		Assert.assertEquals("wrong", 0, testList.size());
		/** ======================================================= */
		for (int i = 0; i < 10; i++) {
			testList2.add(i);
		}
		Assert.assertEquals("wrong", 10, testList2.size());
		testList2.remove(2);
		Assert.assertEquals("wrong", 3, testList2.get(2));
		Assert.assertEquals("wrong", 9, testList2.size());
		testList2.remove(0);
		Assert.assertEquals("wrong", 3, testList2.get(1));
		Assert.assertEquals("wrong", 8, testList2.size());
		Assert.assertEquals("wrong", 1, testList2.get(0));

		testList2.remove(7);
		Assert.assertNull("wrong", testList2.get(7));
		Assert.assertEquals("wrong", 8, testList2.get(6));
		Assert.assertEquals("wrong", 7, testList2.size());

		testList2.remove(8);
		Assert.assertEquals("wrong", 8, testList2.get(6));
		Assert.assertEquals("wrong", 7, testList2.size());

		for (int i = 0; i < 7; i++) {
			testList2.remove(0);
		}
		Assert.assertEquals("wrong", 0, testList2.size());
	}

	@Test
	public void test6() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		Assert.assertFalse("wrong", testList.isEmpty());
		testList.clear();
		Assert.assertTrue("wrong", testList.isEmpty());
		/** ===================================================== */
		for (int i = 0; i < 10; i++) {
			testList2.add(i);
		}
		Assert.assertFalse("wrong", testList2.isEmpty());
		testList2.clear();
		Assert.assertTrue("wrong", testList2.isEmpty());
	}

	@Test
	public void test7() {
		for (int i = 0; i < 10; i++) {
			testList.add(i);
		}
		Assert.assertTrue("wrong", testList.contains(3));
		Assert.assertFalse("wrong", testList.contains(20));
		/** ========================================================= */
		for (int i = 0; i < 10; i++) {
			testList2.add(i);
		}
		Assert.assertTrue("wrong", testList2.contains(3));
		Assert.assertFalse("wrong", testList2.contains(20));
	}
}
