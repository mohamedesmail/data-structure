package eg.edu.alexu.csd.datastructure.linkedList.cs53_64;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

public class doublyList implements ILinkedList {
	public node head;
	public node current;
	public node tail;
	public int size = 0;

	/**
	 * Inserts a specified element at the specified position in the list.
	 */
	public void add(int index, Object element) {
		if (!(index < 0 || index > this.size)) {
			node n = new node();
			n.data = element;
			if (isEmpty()) {
				this.head = n;
				this.tail = n;
				n.next = null;
				this.size++;
			} else if (index == this.size) {
				add(element);
			} else if (index == 0) {
				n.next = this.head;
				this.head.prev = n;
				this.head = n;
				this.size++;
			} else {
				this.current = this.head;
				for (int i = 0; i < this.size; i++) {
					if (i == index - 1) {
						node temp = new node();
						temp = this.current.next;
						this.current.next = n;
						n.prev = this.current.next;
						n.next = temp;
						temp.prev = n;
						this.size++;
						break;
					}
					this.current = this.current.next;
				}
			}
		} else {
			throw null;
		}
	}

	/** ================================================================ */
	/** Inserts the specified element at the end of the list. */
	public void add(Object element) {
		node n = new node();
		n.data = element;
		if (isEmpty()) {
			this.head = n;
			n.next = null;
			this.size++;
			this.tail = n;
		} else {
			n.next = null;
			this.tail.next = n;
			this.tail = n;
			this.size++;
		}
	}

	/** ================================================================ */
	/** Returns the element at the specified position in this list. */
	public Object get(int index) {
		if (this.head == null)
			return null;
		this.current = this.head;
		for (int i = 0; i < this.size; i++) {
			if (i == index) {
				break;
			}
			this.current = this.current.next;
		}
		if (this.current == null)
			throw null;

		return this.current.data;
	}

	/** ================================================================ */
	/**
	 * Replaces the element at the specified position in this list with the
	 * specified element.
	 */
	public void set(int index, Object element) {
		if (!(index < 0 || index > this.size)) {
			this.current = this.head;
			for (int i = 0; i < this.size; i++) {
				if (i == index) {
					this.current.data = element;
					break;
				}
				this.current = this.current.next;
			}
		} else {
			throw null;
		}
	}

	/** ================================================================ */
	/** Removes all of the elements from this list. */
	public void clear() {
		this.head = null;
		this.size = 0;
	}

	/** ================================================================ */
	/** Returns true if this list contains no elements. */
	public boolean isEmpty() {
		return this.head == null;
	}

	/** ================================================================ */
	/** Removes the element at the specified position in this list. */
	public void remove(int index) {
		if (!isEmpty()) {
			if (index == 0) {
				this.head = this.head.next;
				this.size--;
				if (this.head == null) {
					this.tail = null;
				} else {
					this.head.prev = null;
				}
			} else if (index == this.size - 1) {
				this.current = this.head;
				for (int i = 0; i < this.size; i++) {
					if (i == index - 1) {
						break;
					}
					this.current = this.current.next;
				}
				this.tail = this.current;
				this.tail.next = null;
				this.size--;
			} else if ((index > this.size - 1) || (index < 0)) {
				throw null;
			} else {
				node n = new node();
				this.current = this.head;
				for (int i = 0; i < this.size; i++) {
					if (i == index - 1) {
						n = this.current.next;
						this.current.next = n.next;
						n.next.prev = this.current;
						n = null;
						this.size--;
						break;
					}
					this.current = this.current.next;
				}
			}
		} else {
			throw null;
		}
	}

	/** ================================================================ */
	/** Returns the number of elements in this list. */
	public int size() {
		return this.size;
	}

	/** ================================================================ */
	/**
	 * Returns a view of the portion of this list between the specified
	 * fromIndex and toIndex, inclusively.
	 */
	public ILinkedList sublist(int fromIndex, int toIndex) {
		doublyList list = new doublyList();
		if (fromIndex < 0 || fromIndex > this.size || toIndex < fromIndex || toIndex > this.size)
			return null;
		this.current = new node();
		this.current.data = this.head.data;
		this.current.next = new node();
		node x = this.head;
		for (int i = 0; i < fromIndex; i++) {
			this.current.data = x.next.data;
			x = x.next;
		}
		list.add(this.current.data);
		for (int i = fromIndex + 1; i <= toIndex; i++) {
			this.current.next.data = x.next.data;
			list.add(this.current.next.data);
			x = x.next;
			this.current = this.current.next;
			current.next = new node();

		}
		list.tail.next = null;
		return list;
	}

	/** ================================================================ */
	/**
	 * Returns true if this list contains an element with the same value as the
	 * specified element.
	 */
	public boolean contains(Object o) {
		this.current = this.head;
		for (int i = 0; i < this.size; i++) {
			if (this.current.data.equals(o))
				return true;
			this.current = this.current.next;
		}
		return false;
	}
}
