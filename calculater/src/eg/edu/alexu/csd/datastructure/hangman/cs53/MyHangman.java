package eg.edu.alexu.csd.datastructure.hangman.cs53;

import java.util.Random;
import eg.edu.alexu.csd.datastructure.hangman.IHangman;

/**
 * @author first
 *
 */
public class MyHangman implements IHangman {
	private String[] dictionary;
	private String secretWord;
	private String userWord;
	private int maxWrongs, counter = 0;

	/****************************************************/
	/**
	 * Set dictionary words to pick secret words from
	 * 
	 * @param words
	 *            an array of words
	 */
	public void setDictionary(String[] words) {
		dictionary = new String[words.length];
		dictionary = words;
	}

	/***************************************************/
	/**
	 * Pick a random secret word from dictionary and returns it
	 * 
	 * @return secret word
	 */
	public String selectRandomSecretWord() {
		if (dictionary != null) {
			Random rand = new Random();
			int i = dictionary.length;
			int n = rand.nextInt(i);
			secretWord = dictionary[n];
			secretWord = secretWord.toUpperCase();
			char[] wordArr = new char[secretWord.length()];
			for (int j = 0; j < secretWord.length(); j++) {
				wordArr[j] = '-';
			}
			userWord = String.valueOf(wordArr);
		}
		return secretWord;
	}

	/********************************************************/
	/**
	 * Receive a new user guess, and verify it against the secret word.
	 * 
	 * @param c
	 *            case insensitive user guess. If c is NULL then ignore it and
	 *            do no change
	 * @return secret word with hidden characters (use '-' instead unsolved
	 *         characters), or return NULL if user reached max wrong guesses
	 */
	public String guess(Character c) {
		char[] wordArr = new char[secretWord.length()];
		if (userWord != null) {
			wordArr = userWord.toCharArray();
			c = Character.toUpperCase(c);
			boolean correct = false;
			if (c != null) {
				for (int i = 0; i < secretWord.length(); i++) {
					if (secretWord.charAt(i) == c) {
						wordArr[i] = c;
						correct = true;
					}
				}
				userWord = String.valueOf(wordArr);
			}
			if (!correct) {
				counter++;
			}
			if (counter == maxWrongs) {
				userWord = null;
			}
		}
		return userWord;
	}

	/***************************************************/
	/**
	 * Set the maximum number of wrong guesses
	 * 
	 * @param max
	 *            maximum number of wrong guesses, If is NULL, then assume it 0
	 */
	public void setMaxWrongGuesses(Integer max) {
		// TODO Auto-generated method stub
		if (max == null) {
			maxWrongs = 0;
		} else {
			maxWrongs = max;
		}
	}
}
