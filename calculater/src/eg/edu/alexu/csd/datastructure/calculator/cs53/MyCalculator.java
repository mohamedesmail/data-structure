package eg.edu.alexu.csd.datastructure.calculator.cs53;

import eg.edu.alexu.csd.datastructure.calculator.ICalculator;

/**
 * @author first
 *
 */
public class MyCalculator implements ICalculator {
	/**
	 * Adds given two numbers
	 * 
	 * @param x
	 *            first number
	 * @param y
	 *            second number
	 * @return the sum of the two numbers
	 */
	public int add(int x, int y) {
		int add;
		add = x + y;
		return add;
	}

	/**
	 * Divides two numbers
	 * 
	 * @param x
	 *            first number
	 * @param y
	 *            second number
	 * @return the division result
	 */
	public float divide(int x, int y) {
		float divide;
		divide = (float) x / (float) y;
		return divide;
	}
}
