package eg.edu.alexu.csd.datastructure.queue.cs53;

import eg.edu.alexu.csd.datastructure.queue.IQueue;
import eg.edu.alexu.csd.datastructure.queue.IArrayBased;

public class ArrQueue implements IQueue, IArrayBased {
	private int first = -1;
	private int end = -1;
	private int arrSize;
	private int size = 0;
	private Object[] arr;

	public ArrQueue(int n) {
		arrSize = n;
		arr = new Object[arrSize];
	}

	/**
	 * Inserts an item at the queue front.
	 */
	public void enqueue(Object item) {
		if (this.size == this.arrSize) {
			throw null;
		} else {
			this.size++;
			if (this.end == this.arrSize - 1) {
				this.end = 0;
				this.arr[this.end] = item;
			} else {
				this.end++;
				this.arr[this.end] = item;
			}
		}
	}

	/**
	 * Removes the object at the queue rear and returns it.
	 */
	public Object dequeue() {
		if (this.size == 0) {
			throw null;
		} else {
			this.size--;
			if (this.first == this.arrSize - 1) {
				this.first = 0;
			} else {
				this.first++;
			}
		}
		return arr[first];
	}

	/**
	 * Tests if this queue is empty.
	 */
	public boolean isEmpty() {
		boolean x = this.size == 0;
		return x;
	}

	/**
	 * Returns the number of elements in the queue
	 */
	public int size() {
		return this.size;
	}
}
