package eg.edu.alexu.csd.datastructure.queue.cs53;

import eg.edu.alexu.csd.datastructure.queue.IQueue;
import eg.edu.alexu.csd.datastructure.queue.ILinkedBased;
import eg.edu.alexu.csd.datastructure.linkedList.cs53_64.doublyList;

public class LinkedQueue implements IQueue, ILinkedBased {

	doublyList LQ = new doublyList();

	/**
	 * Inserts an item at the queue front.
	 */
	public void enqueue(Object item) {
		LQ.add(item);
	}

	/**
	 * Removes the object at the queue rear and returns it.
	 */
	public Object dequeue() {
		Object element;
		if (LQ.isEmpty()) {
			throw null;
		} else {
			element = LQ.get(0);
			LQ.remove(0);
		}
		return element;
	}

	/**
	 * Tests if this queue is empty.
	 */
	public boolean isEmpty() {

		return LQ.isEmpty();
	}

	/**
	 * Returns the number of elements in the queue
	 */
	public int size() {
		return LQ.size();
	}
}
