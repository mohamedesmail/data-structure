package eg.edu.alexu.csd.datastructure.maze.cs53;

import java.util.Scanner;
import eg.edu.alexu.csd.datastructure.queue.cs53.LinkedQueue;
import eg.edu.alexu.csd.datastructure.linkedList.cs53_64.doublyList;
import eg.edu.alexu.csd.datastructure.stack.cs53.MyStack;
import eg.edu.alexu.csd.datastructure.maze.IMazeSolver;

public class MySolver implements IMazeSolver {
	// public MNode ;
	private int n;
	private int m;
	public Scanner read;
	MapN[][] mapArr;
	MyStack stack = new MyStack();
	LinkedQueue queue = new LinkedQueue();
	MNode start = new MNode();
	//MNode end = new MNode();
	MNode end = null;
	doublyList endQueue = new doublyList();

	/**
	 * Read the maze file, and solve it using Breadth First Search
	 * 
	 * @param maze
	 *            maze file
	 * @return the coordinates of the found path from point 'S' to point 'E'
	 *         inclusive, or null if no path found. coordinates indexes are zero
	 *         based.
	 */
	public int[][] solveBFS(java.io.File maze) {
		try {
			read = new Scanner(maze);
		} catch (Exception e) {
			System.out.println("not founded");
			throw null;
		}
		String temp = read.nextLine();
		String temp1 = new String();
		int i;
		for (i = 0; i < temp.length(); i++) {
			if (temp.charAt(i) >= '0' && temp.charAt(i) <= '9') {
				temp1 += temp.charAt(i);
			} else if (temp.charAt(i) == ' ') {
				n = Integer.parseInt(temp1);
				temp1 = new String();
				break;
			}
		}
		for(int j = i+1; j < temp.length(); j++){
			temp1 += temp.charAt(j);
		}		
		m = Integer.parseInt(temp1);
		boolean hasS = false;
		boolean hasE = false;
		mapArr = new MapN[n][m];
		for ( i = 0; i  < n; i++){
			temp = read.nextLine();
			for (int j = 0; j  < m; j++){
				mapArr[i][j] = new MapN();
				char c1 = temp.charAt(j);
				mapArr[i][j].value = c1;
				if(c1 == 'S'){
					start.col = j;
					start.row = i;
					hasS = true;
				}
				if(c1 == 'E'){
					hasE = true;
				}
			}
		}	
		if(!hasS || !hasE){
			throw null;
		}
		if(read.hasNextLine() || temp.length() != m){
			throw null;
		}
		read.close();
		
		
		MNode point = new MNode();
		point = start;		
		queue.enqueue(point);
		mapArr[start.row][start.col].isVisited = true;
		while(!queue.isEmpty()){
			point = (MNode)queue.dequeue();
			mapArr[point.row][point.col].isVisited = true;
			char c2 = mapArr[point.row][point.col].value;
			if(c2 == 'E'){
				end = point;
				//endQueue.add(point);
				break;
			}else{
				int RRow,RCol,LRow,LCol,TRow,TCol,DRow,DCol;
				TRow = point.row-1;
				TCol = point.col;
				if(TRow < n && TCol < m && TRow != -1){
					char c3 = mapArr[TRow][TCol].value;
					boolean visited = mapArr[TRow][TCol].isVisited;
					if(c3 != '#' && !visited){
						MNode tempP = new MNode();
						tempP.row = TRow;
						tempP.col = TCol;
						tempP.parent = point;
						queue.enqueue(tempP);
					}
				}
				
				/****/
				DRow = point.row+1;
				DCol = point.col;
				if(DRow < n && DCol < m){
					char c3 = mapArr[DRow][DCol].value;
					boolean visited = mapArr[DRow][DCol].isVisited;
					if(c3 != '#' && !visited){
						MNode tempP = new MNode();
						tempP.row = DRow;
						tempP.col = DCol;
						tempP.parent = point;
						queue.enqueue(tempP);
					}
				}
				RRow = point.row;
				RCol = point.col+1;
				if(RRow < n && RCol < m){
					char c3 = mapArr[RRow][RCol].value;
					boolean visited = mapArr[RRow][RCol].isVisited;
					if(c3 != '#' && !visited){
						MNode tempP = new MNode();
						tempP.row = RRow;
						tempP.col = RCol;
						tempP.parent = point;
						queue.enqueue(tempP);
					}
				}
				/*****/
				LRow = point.row;
				LCol = point.col-1;
				if(LRow < n && LCol < m && LCol != -1){
					char c3 = mapArr[LRow][LCol].value;
					boolean visited = mapArr[LRow][LCol].isVisited;
					if(c3 != '#' && !visited){
						MNode tempP = new MNode();
						tempP.row = LRow;
						tempP.col = LCol;
						tempP.parent = point;
						queue.enqueue(tempP);
					}
				}
											
			}			
		}
		if(end == null){
			return null;
			//throw null;
		}
		/*if(endQueue.size() == 0){
			return null;
			//throw null;
		}
		int size = endQueue.size();
		int min  = 10000;
		for(int p = 0; p < size;p++){
			int count = 0;
			MNode temp2 = (MNode)endQueue.get(0);
			MNode temp3 = temp2;
			while(temp2.parent != null){			
				temp2 = temp2.parent;
				count++;
			}
			if(count < min){
				min = count;
				end = temp3;
			}
		}*/
		MNode tempP = end;
		doublyList list = new doublyList();
		list.add(0, tempP);
		while(tempP.parent != null){			
			tempP = tempP.parent;
			list.add(0, tempP);
		}
		int[][] path =new int[list.size][2];
		int size1 = list.size();
		for(int k = 0; k < size1; k++){
			tempP = (MNode)list.get(0);
			list.remove(0);
			path[k][0] = tempP.row;
			path[k][1] = tempP.col;
		}
		return path;
		//throw null;
		//return null;
	}
	
	/**
	* Read the maze file, and solve it using Depth First Search
	* @param maze maze file
	* @return the coordinates of the found path from point 'S'
	* to point 'E' inclusive, or null if no path found.
	* coordinates indexes are zero based.
	*/
	public int[][] solveDFS(java.io.File maze){
		try {
			read = new Scanner(maze);
		} catch (Exception e) {
			System.out.println("not founded");
			throw null;
		}
		String temp = read.nextLine();
		String temp1 = new String();
		int i;
		for (i = 0; i < temp.length(); i++) {
			if (temp.charAt(i) >= '0' && temp.charAt(i) <= '9') {
				temp1 += temp.charAt(i);
			} else if (temp.charAt(i) == ' ') {
				n = Integer.parseInt(temp1);
				temp1 = new String();
				break;
			}
		}
		for(int j = i+1; j < temp.length(); j++){
			temp1 += temp.charAt(j);
		}	
		m = Integer.parseInt(temp1);
		
		mapArr = new MapN[n][m];
		boolean hasS = false;
		boolean hasE = false;
		for ( i = 0; i  < n; i++){
			temp = read.nextLine();
			for (int j = 0; j  < m; j++){
				mapArr[i][j] = new MapN();
				char c1 = temp.charAt(j);
				mapArr[i][j].value = c1;
				if(c1 == 'S'){
					start.col = j;
					start.row = i;
					hasS = true;
				}
				if(c1 == 'E'){
					hasE = true;
				}
			}
		}	
		if(!hasS || !hasE){
			throw null;
		}
		if(read.hasNextLine() || temp.length() != m){
			throw null;
		}
		read.close();
		
		MNode point = new MNode();
		point = start;		
		stack.push(point);
		mapArr[start.row][start.col].isVisited = true;
		while(!stack.isEmpty()){
			point = (MNode)stack.pop();
			mapArr[point.row][point.col].isVisited = true;
			char c2 = mapArr[point.row][point.col].value;
			if(c2 == 'E'){
				end = point;
				break;
			}else{
				int RRow,RCol,LRow,LCol,TRow,TCol,DRow,DCol;
				LRow = point.row;
				LCol = point.col-1;
				if(LRow < n && LCol < m && LCol != -1){
					char c3 = mapArr[LRow][LCol].value;
					boolean visited = mapArr[LRow][LCol].isVisited;
					if(c3 != '#' && !visited){
						MNode tempP = new MNode();
						tempP.row = LRow;
						tempP.col = LCol;
						tempP.parent = point;
						stack.push(tempP);
					}
				}
				/*******/
				TRow = point.row-1;
				TCol = point.col;
				if(TRow < n && TCol < m && TRow != -1){
					char c3 = mapArr[TRow][TCol].value;
					boolean visited = mapArr[TRow][TCol].isVisited;
					if(c3 != '#' && !visited){
						MNode tempP = new MNode();
						tempP.row = TRow;
						tempP.col = TCol;
						tempP.parent = point;
						stack.push(tempP);
					}
				}
				/******/
				RRow = point.row;
				RCol = point.col+1;
				if(RRow < n && RCol < m){
					char c3 = mapArr[RRow][RCol].value;
					boolean visited = mapArr[RRow][RCol].isVisited;
					if(c3 != '#' && !visited){
						MNode tempP = new MNode();
						tempP.row = RRow;
						tempP.col = RCol;
						tempP.parent = point;
						stack.push(tempP);
					}
				}
				/******/
				DRow = point.row+1;
				DCol = point.col;
				if(DRow < n && DCol < m){
					char c3 = mapArr[DRow][DCol].value;
					boolean visited = mapArr[DRow][DCol].isVisited;
					if(c3 != '#' && !visited){
						MNode tempP = new MNode();
						tempP.row = DRow;
						tempP.col = DCol;
						tempP.parent = point;
						stack.push(tempP);
					}
				}
			}			
		}
		if(end == null){
			return null;
		}
		MNode tempP = end;
		doublyList list = new doublyList();
		list.add(0, tempP);
		while(tempP.parent != null){			
			tempP = tempP.parent;
			list.add(0, tempP);
		}
		int[][] path =new int[list.size][2];
		int size = list.size();
		for(int k = 0; k < size; k++){
			tempP = (MNode)list.get(0);
			list.remove(0);
			path[k][0] = tempP.row;
			path[k][1] = tempP.col;
		}
		return path;
	}

}
